﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class MatrixOperation : Operation
    {

        int Matrix1Rows;
        int Matrix1Cols;

        int Matrix2Rows;
        int Matrix2Cols;

        double[,] Matrix1;
        double[,] Matrix2;
        double[,] Result;


        protected override void InitNumbers()
        {
            Console.WriteLine("Размерность первой матрицы:");
            Matrix1Rows = Convert.ToInt16(GetNumber("Введите количество строк первой матрицы:", false));
            Matrix1Cols = Convert.ToInt16(GetNumber("Введите количество колонок первой матрицы:", false));

            Console.WriteLine("Размерность второй матрицы:");
            Matrix2Rows = Convert.ToInt16(GetNumber("Введите количество строк второй матрицы:", false));
            Matrix2Cols = Convert.ToInt16(GetNumber("Введите количество колонок второй матрицы:", false));
            if (Matrix1Cols != Matrix2Rows)
            {
                Console.WriteLine("Ошибка - количество колонок первой матрицы и количество строк второй матрицы не равны");
                Console.WriteLine("Введите значения заново");
                InitNumbers();
            }
            else
            {
                Matrix1 = EnterMatrix("Ввод элементов первой матрицы:", Matrix1Rows, Matrix1Cols);
                Matrix2 = EnterMatrix("Ввод элементов второй матрицы:", Matrix2Rows, Matrix2Cols);                
            }
        }

        protected override void DoCalculation()
        {
            Result = MultiplyMatrix(Matrix1, Matrix2, Matrix1Rows, Matrix2Cols, Matrix1Cols);
        }

        protected override void PrintResult()
        {
            PrintMatrix("Результат умножения:", Result);
        }

        private double[,] EnterMatrix(string title, int MatrixRows, int MatrixCols)
        {
            double[,] Matrix = new double[MatrixRows, MatrixCols];
            Console.WriteLine(title);
            for (int i = 0; i < MatrixRows; i++)
            {
                Console.WriteLine("Заполнение строки {0}", i + 1);
                for (int j = 0; j < MatrixCols; j++)
                {
                    Matrix[i, j] = GetNumber("Введите значения колонки " + (j + 1) + ":", true);
                }
            }
            PrintMatrix("Введенная матрица", Matrix);
            return Matrix;
        }

        private void PrintMatrix(string title, double[,] matrix)
        {
            Console.WriteLine(title);
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        private double[,] MultiplyMatrix(double[,] Matrix1, double[,] Matrix2, int Matrix1Rows, int Matrix2Cols, int Matrix1Cols)
        {
            double[,] result = new double[Matrix1Rows, Matrix2Cols];
            for (int i = 0; i < Matrix1Rows; i++)
            {
                for (int j = 0; j < Matrix2Cols; j++)
                {
                    result[i, j] = 0;
                    for (int k = 0; k < Matrix1Cols; k++)
                    {
                        result[i, j] = result[i, j] + Matrix1[i, k] * Matrix2[k, j];
                    }
                }
            }
            return result;
        }
    }
}
