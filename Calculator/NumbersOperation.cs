﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public abstract class NumbersOperation : Operation
    {
        Boolean PrevResult = false;
        protected double firstNumber;
        protected double secondNumber;
        protected string stringOperation;
        protected double Result;

        public NumbersOperation()
        {

        }

        public NumbersOperation(double? prevValue)
        {
            if (prevValue != null)
            {
                this.firstNumber = prevValue.Value;
                this.PrevResult = true;
            }

        }

        public double GetResult()
        {
            return Result;
        }

        protected abstract string GetOperationSymbol();

        protected override void PrintResult()
        {
            Console.WriteLine("Результат {0} {1} {2} = {3}", this.firstNumber, GetOperationSymbol(), this.secondNumber, GetResult());
            History.WriteHistory(this.firstNumber + " " + GetOperationSymbol() + " " + this.secondNumber + " = " + GetResult() + "\n");
        }

        protected override void InitNumbers()
        {
            if (!PrevResult)
            {
                firstNumber = GetNumber("Введите первое число : ", true);
            }

            this.secondNumber = GetNumber("Введите второе число: ", true);
        }
    }
}
