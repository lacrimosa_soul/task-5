﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public abstract class Operation
    {
        public Operation()
        {

        }

        protected abstract void InitNumbers();

        protected abstract void DoCalculation();

        protected abstract void PrintResult();
        public void Calculate()
        {
            InitNumbers();
            DoCalculation();
            PrintResult();
        }

        protected double GetNumber(string outputText, Boolean needZero)
        {
            double parse;
            Console.WriteLine(outputText);
            string tempInput = Console.ReadLine();
            if (!double.TryParse(tempInput, out parse))
            {
                Console.WriteLine("Неверное значение! Введите число:");
                return GetNumber(outputText, needZero);
            }
            if (!needZero && parse == 0)
            {
                Console.WriteLine("Значение должно быть больше 0:");
                return GetNumber(outputText, needZero);
            }
            return double.Parse(tempInput);
        }
    }
}
