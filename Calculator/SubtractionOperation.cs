﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class SubtractionOperation : NumbersOperation
    {

        public SubtractionOperation(double? prevValue) : base(prevValue)
        {

        }

        protected override void DoCalculation()
        {
            Result = firstNumber - secondNumber;
        }

        protected override string GetOperationSymbol()
        {
            return "-";
        }
    }
}
