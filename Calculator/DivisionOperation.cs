﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class DivisionOperation : NumbersOperation
    {

        public DivisionOperation(double? prevValue) : base(prevValue)
        {

        }

        protected override void DoCalculation()
        {
            if (secondNumber == 0)
            {
                secondNumber = GetNumber("Деление на 0 невозможно, введите другое число", false);
            }
            Result = firstNumber / secondNumber;
        }

        protected override string GetOperationSymbol()
        {
            return "/";
        }
    }
}
