﻿using System;

namespace Calculator
{
    class Program
    {

        static string[] Operations = { "+", "-", "*", "/" };
        static string[] mainCommands = { "calc", "matrix", "history", "menu", "exit" };
        static string[] CalcCommands = { "y", "n", "menu" };

        static void Main(string[] args)
        {
            Boolean exit = false;
            DisplayMenu();
            while (!exit)
            {
                string command = Commands.GetCommand("Введите команду:", mainCommands);
                switch (command)
                {
                    case "calc":
                        NumbersCalculation(null);
                        break;
                    case "matrix":
                        Operation operation = new MatrixOperation();
                        operation.Calculate();
                        break;
                    case "history":
                        History.PrintHistory();
                        break;
                    case "menu":
                        DisplayMenu();
                        break;
                    case "exit":
                        exit = true;
                        break;
                }
            }

            Console.WriteLine("Работа завершена, нажмите любую клавишу:");
            Console.ReadKey();
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("Калькулятор - главное меню");
            Console.WriteLine("Позволяет производить математические операции +, -, *, / над числами");
            Console.WriteLine("Позволяет производить умножение матриц");
            Console.WriteLine("Позволяет сохранять предыдущий результат и использовать в следующем вычислении");
            Console.WriteLine("Список первоначальных команд:");
            Console.WriteLine("calc - режим работы с числами");
            Console.WriteLine("matrix - режим работы с матрицами");
            Console.WriteLine("history - история вычислений");
            Console.WriteLine("menu - главное меню");
            Console.WriteLine("exit - завершение работы");
        }

        private static void NumbersCalculation(double? prevResult)
        {

            NumbersOperation operation = GetNumbersOperation(prevResult);
            operation.Calculate();
            double result = operation.GetResult();
            Console.WriteLine("Продолжить вычисление далее (используя предыдущий результат) - введите y");
            Console.WriteLine("Начать новое вычисление - введите n");
            Console.WriteLine("Главное меню - введите menu");
            string command = Commands.GetCommand("Введите команду:", CalcCommands);
            switch (command)
            {
                case "y":
                    NumbersCalculation(result);
                    break;
                case "n":
                    NumbersCalculation(null);
                    break;
                case "menu":
                    DisplayMenu();
                    break;
            }
        }

        private static NumbersOperation GetNumbersOperation(double? prevResult)
        {
            string stringOperation = Commands.GetCommand(
                    "Введите операцию: +, -, *, / :", Operations);
            NumbersOperation operation = null;
            switch (stringOperation)
            {
                case "+":
                    operation = new AdditionOperation(prevResult);
                    break;
                case "-":
                    operation = new SubtractionOperation(prevResult);
                    break;
                case "*":
                    operation = new MultiplicationOperation(prevResult);
                    break;
                case "/":
                    operation = new DivisionOperation(prevResult);
                    break;
            }
            return operation;
        }
    }
}

