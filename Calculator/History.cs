﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class History
    {

        private static string history;

        public static void WriteHistory(string history)
        {
            History.history += history;
        }

        public static void PrintHistory()
        {
            Console.WriteLine(History.history);
        }
    }
}
