﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public class Commands
    {
        public static string GetCommand(string outputText, string[] commands)
        {
            Console.WriteLine(outputText);
            string tempInput = Console.ReadLine();
            while (!IsValidCommand(tempInput, commands))
            {
                Console.WriteLine("Неверная команда - {0}", tempInput);
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }

        private static bool IsValidCommand(string input, string[] available)
        {
            foreach (String command in available)
            {
                if (command.Equals(input))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
